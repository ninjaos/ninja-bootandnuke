#!/usr/bin/ash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#                  +Ninja Boot'n'Nuke (nban)+
#  Inspired by Darik's Boot'n'Nuke, but implemented soley as an Arch Linux
#  mkinitcpio script.
#
#  Nban in turn, is the core of the "Ninja Shiruken", with the addition of a
#  syslinux bootloader, is a single use(read self wiping) nban flash drive,
#  which can be quickly spawned by Ninja OS.

# you can test this with mkinitcpio -p nban in ninja os, and then booting with
# initrd=nban.img at the kernel command line. Careful, this will wipe all data
# from all attached storage media.

FILL_SRC=/dev/zero
RAND_SRC=/dev/urandom
PIDS=""

## As adapted from self destruct.
nuke_dev() {
  # Erase a block device. $1 is the device name, $2 is the type, either disk
  # or flash. This is used for wipe_part_headers() see bellow
  local disk=$1
  local size="$(get_size ${disk})"

  echo "Disk ${disk}(${size}):		Starting Wipe..."

  # Start by wiping the headers
  wipe_part_headers "${disk}"
  # Wipe the entire disk, front to back
  wipe_methods "${disk}"
  if [ $? -eq 0 ];then
    echo "Disk ${disk}(${size}):		Fill Complete!"
   else
    echo "Wipe of ${disk}(${size}) failed"
  fi
  sync
  fdisk_final ${disk}
  sync
  echo "Disk ${disk}(${size}):		Done!"
}

wipe_methods(){
  # now fill the entire disk. Try using blkdiscard first, then fall back to dd
  local disk="${1}"
  blkdiscard -s "${disk}"
  [ ${?} -eq 0 ] && return 0
  blkdiscard "${disk}"
  [ ${?} -eq 0 ] && return 0
  dd if=${FILL_SRC} of="${disk}" bs=128k 2> /dev/null
  [ ${?} -ne 0 ] && return 1
}

wipe_part_headers() {
  # This script wipes any headers including the boot sector with random as
  # defined in $RAND_SRC. The $1 is the name of the parent block device. Should
  # kill any crypto keys and partition tables 
  local disk="${1}"
  local parts=$(sfdisk -l ${disk} -o Device -q |grep -v Device)
  for part in ${parts};do
    dd if=${RAND_SRC} bs=128k count=1 of=${part} 2> /dev/null
  done
  dd if=${RAND_SRC} bs=128k count=1 of=${disk} 2> /dev/null

  sync
}

get_size() {
  # takes $1 as a block device and returns the size from fdisk.
  dev_size=$(fdisk -l ${1} |head -1 |cut -f 3-4 -d " "|cut -d "," -f 1 )
  if [ "${dev_size}x" = "x" ];then
    echo 0
   else
    echo "${dev_size}"
  fi
}
fdisk_final() {
fdisk ${1} &> /dev/null << EOF
n
p
1


w
EOF
mkfs.vfat ${1}1 &> /dev/null
}

main(){
  # Trap escapes, so this cannot be interupted. This really isn't needed as we
  # don't have the keyboard drivers in nban anyway
  trap "true" 1 2 9 15 17 19 23
  # alternate trap is to reboot on interrupt
  # trap "wait 2;reboot -f" 1 2 9 15 17 19 23

  # wait 2 seconds to give all devices time to initialize
  sleep 2

  # get list of attached storage
  DEVICES=$(lsblk -lpn -o name -d -e 230)
  set ${DEVICES}
  NUMDEVS=${#}

  #tell the user what we are doing.
  echo "		+++Ninja Boot'n'Nuke+++"
  echo -n "Found ${NUMDEVS} device(s):"
  for DEVICE in ${@};do
    SIZE=$(get_size ${DEVICE})
    echo -n " ${DEVICE}(${SIZE})"
  done
  unset SIZE
  echo " "; echo " "

  for DEVICE in ${DEVICES} ;do
    ( nuke_dev ${DEVICE} ) &
    PIDS="$PIDS $!"
  done

  #Wait for all the wipes to be done
  #wait ${PIDS}
  wait
  echo "Complete! Rebooting"
  sleep 3

  # when we are all done reboot
  reboot -f &
  #backstop to prevent a return to shell pending reboot.
  while true ;do
    sleep 2
  done
}

main ${@}
