Ninja Boot'n'Nuke
=================
Ninja OS subproject. Implementation of a Boot'n'Nuker completely using initcpio
native tools.

This project has been backported to work with vanilla Arch linux. This is
basicly an initcpio profile and some extra toolings.

We will fill this out later.

mkinitcpio -p nban - build the initcpio image

Shurikens
---------
shuriken_forge - is a tool for making 'shuriken's a boot and nuker on a flash
stick. see the man page for more info
